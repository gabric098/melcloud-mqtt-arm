ARG NODE_VERSION=16.13
ARG OS=alpine3.12
FROM node:${NODE_VERSION}-${OS}

WORKDIR /app

COPY package.json .
COPY package-lock.json .
COPY tsconfig.json .
COPY LICENSE .

RUN npm install

COPY src/ ./src/

RUN npm run build

CMD [ "node", "build/src/index.js" ]