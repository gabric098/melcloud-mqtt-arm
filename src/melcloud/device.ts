import { EventEmitter } from 'events';
import { IGetDeviceResponse } from './api/getDevice';
import { IDevice, ILocation } from './api/listDevices';
import { MelCloud } from './index';

const operationMode = {
  heat: 1,
  dry: 2,
  cool: 3,
  fan: 7,
  auto: 8,
};

enum EDeviceMode {
  HEAT = 'heat',
  COOL = 'cool',
  FAN = 'fan',
  AUTO = 'auto',
  DAY = 'dry',
}
interface IDeviceSchedule {
  prev: string;
  next: string;
}

interface IDeviceLocalState {
  status: {
    online: boolean;
    sync?: boolean;
    temperature: number;
  };
  state: {
    power: boolean;
    target: number;
    fan: string | number;
    mode: string | undefined; // TODO: this could be made as string default
    horizontal: string | number | undefined; // TODO: this could be made as string default
    vertical: string | number | undefined; // TODO: this could be made as string default
  };
}

export interface IPrepareUpdate {
  Power: boolean | undefined;
  SetTemperature: number | undefined;
  SetFanSpeed: number;
  OperationMode: number;
  VaneHorizontal: number | undefined;
  VaneVertical: number | undefined;
}

const parseVane =
  (swing: number, values = 5) =>
  (v: number) => {
    if (v === 0) return 'auto';

    if (v === swing) return 'swing';

    // if we have 5 positions, then
    // 1 = 0%
    // 2 = 25%
    // 3 = 50%
    // 4 = 75%
    // 5 = 100%
    if (v >= 1 && v <= values) return (v - 1) / (values - 1);

    return undefined;
  };

const prepareVane =
  (swing: number, values = 5) =>
  (v: number | 'auto' | 'swing') => {
    if (v === 'auto') return 0;

    if (v === 'swing') return swing;

    if (v >= 0 && v <= 1) return Math.round(v * (values - 1) + 1);

    return undefined;
  };

const parseVertical = parseVane(7);
const prepareVertical = prepareVane(7);
const parseHorizontal = parseVane(12);
const prepareHorizontal = prepareVane(12);

const diff = (
  a: Record<string, unknown>,
  b: Record<string, unknown>,
): Record<string, unknown> =>
  [...Object.keys(a), ...Object.keys(b)].reduce((result, k) => {
    if (a[k] === b[k]) return result;

    const change =
      a[k] !== null && typeof a[k] === 'object'
        ? diff(a[k] as Record<string, unknown>, b[k] as Record<string, unknown>)
        : b[k];

    if (
      change !== null &&
      typeof change === 'object' &&
      Object.keys(change as Record<string, unknown>).length === 0
    ) {
      return result;
    }

    (result as Record<string, unknown>)[k] = change;

    return result;
  }, {});

interface IDeviceInfo {
  id: number;
  name: string;
  serial: string;
  mac: string;
  building: number;
  lastSeen: string;
  address: string;
  location: {
    latitude: number;
    longitude: number;
  };
}
interface IDeviceState {
  mode: EDeviceMode;
  power?: string;
  target: number;
  fan: 'auto' | number;
  horizontal: number;
  vertical: number;
}

interface IDeviceAbility {
  speeds: number;
}
export class Device extends EventEmitter {
  cloud: MelCloud;

  state: IDeviceLocalState | undefined;

  rawState: IGetDeviceResponse | undefined;

  data: IDevice;

  location: ILocation;

  id: number;

  building: number;

  info: IDeviceInfo;

  interval: ReturnType<typeof setInterval> | undefined = undefined;

  ability: IDeviceAbility = { speeds: 0 };

  schedule: IDeviceSchedule | undefined;

  contextKey: string;

  constructor(
    cloud: MelCloud,
    device: IDevice,
    location: ILocation,
    contextKey: string,
  ) {
    super();

    this.state = undefined;
    this.cloud = cloud;
    this.data = device;
    this.location = location;
    this.id = device.DeviceID;
    this.building = device.BuildingID;
    this.contextKey = contextKey;

    this.info = {
      id: device.DeviceID,
      name: device.DeviceName,
      serial: device.SerialNumber,
      mac: device.MacAddress,
      building: device.BuildingID,
      lastSeen: device.Device.LastTimeStamp,
      address: [location.AddressLine1, location.AddressLine2].join('\n'),
      location: {
        latitude: location.Latitude,
        longitude: location.Longitude,
      },
    };
  }

  async initialize() {
    await this.read();
    this.interval = setInterval(async () => {
      await this.read();
    }, this.cloud.interval);
  }

  async read() {
    const deviceState = await this.cloud.getDevice(this.id, this.building);
    if (deviceState) {
      this.update(deviceState);
    }
  }

  prepareUpdate(state: IDeviceState, ability: IDeviceAbility): IPrepareUpdate {
    const { mode } = state;
    return {
      Power: state.power !== undefined ? !!state.power : undefined,
      SetTemperature: state.target
        ? Math.round(state.target * 2) / 2
        : undefined,
      SetFanSpeed:
        state.fan === 'auto' ? 0 : Math.floor(state.fan * ability.speeds),
      OperationMode: operationMode[mode],
      VaneHorizontal: prepareHorizontal(state.horizontal),
      VaneVertical: prepareVertical(state.vertical),
    };
  }

  parseState(state: IGetDeviceResponse): IDeviceLocalState {
    return {
      status: {
        online: !state.Offline,
        sync: !state.HasPendingCommand,
        temperature: state.RoomTemperature,
      },
      state: {
        power: state.Power,
        target: state.SetTemperature,
        fan:
          state.SetFanSpeed === 0
            ? 'auto'
            : state.SetFanSpeed / state.NumberOfFanSpeeds,
        mode: Object.keys(operationMode).find(
          (k) => operationMode[k as EDeviceMode] === state.OperationMode,
        ),
        horizontal: parseHorizontal(state.VaneHorizontal),
        vertical: parseVertical(state.VaneVertical),
      },
    };
  }

  parseSchedule(state: IGetDeviceResponse): IDeviceSchedule {
    return {
      prev: new Date(`${state.LastCommunication}Z`).toISOString(),
      next: new Date(`${state.NextCommunication}Z`).toISOString(),
    };
  }

  parseAbility(state: IGetDeviceResponse): IDeviceAbility {
    return {
      speeds: state.NumberOfFanSpeeds,
    };
  }

  update(state: IGetDeviceResponse) {
    // cleanup
    // TODO: implement adapters
    delete state.WeatherObservations;

    // debug information
    const rawState = this.rawState || {};
    const diffRaw = diff(rawState, state as unknown as Record<string, unknown>);
    if (diffRaw && Object.keys(diffRaw).length > 0) {
      this.emit('debug', diffRaw);
    }

    this.rawState = state;

    const next = this.parseState(state);

    next.status = {
      ...(next.status.sync ? next.state : this.state),
      ...next.status,
    };

    delete next.status.sync;

    const nextSchedule = this.parseSchedule(state);

    this.ability = this.parseAbility(state);

    if (!this.state) {
      this.emit('connect', {
        ...this.info,
        ...this.ability,
      });

      this.emit('state', next.state, next.state);
      this.emit('status', next.status, next.status);
      this.emit('schedule', nextSchedule, nextSchedule);

      this.state = next;

      return this.state;
    }

    const schedule = this.schedule || {};
    const diffSchedule = diff(
      schedule,
      nextSchedule as unknown as Record<string, unknown>,
    );
    if (diffSchedule && Object.keys(diffSchedule).length > 0) {
      this.schedule = nextSchedule;
      this.emit('schedule', this.schedule, diffSchedule);
    }

    const diffState = diff(this.state.state, next.state);
    if (diffState && Object.keys(diffState).length > 0) {
      this.state.state = next.state;
      this.emit('state', this.state.state, diffState);
    }

    const diffStatus = diff(this.state.status, next.status);
    if (diffStatus && Object.keys(diffStatus).length > 0) {
      this.state.status = next.status;
      this.emit('status', this.state.status, diffStatus);
    }

    return this.state;
  }

  async set(update: IDeviceState) {
    const change = this.prepareUpdate(update, this.ability);

    const same = Object.entries(change).every(
      ([key, value]) =>
        value === undefined ||
        (this.rawState as unknown as Record<string, unknown>)[key] === value,
    );

    if (same) return Promise.resolve();

    Object.entries(change).forEach(([key, value]) => {
      if (value === undefined) {
        (change as unknown as Record<string, unknown>)[key] = (
          this.rawState as unknown as Record<string, unknown>
        )[key];
      }
    });

    const response = await this.cloud.setAta(this.id, change);
    if (response) {
      this.update(response);
    }
  }
}

module.exports = { Device };
