import fetch from 'cross-fetch';
import { EventEmitter } from 'events';
import { MEL_CLOUD_APP_VERSION } from '../constants';
import {
  IClientLoginRequest,
  IClientLoginResponse,
  ILoginData,
} from './api/login';
import { bodyParamsToFormPost } from './utils';
import { Device, IPrepareUpdate } from './device';
import { IDevice, ILocation } from './api/listDevices';
import { IGetDeviceResponse } from './api/getDevice';

const timeout = (time: number) =>
  new Promise((resolve) => setTimeout(resolve, time));

interface IMelCloudParams {
  username: string;
  password: string;
  interval: number;
}

export class MelCloud extends EventEmitter {
  devices: Device[];

  interval: number;

  username: string;

  password: string;

  currentSessionData: ILoginData | undefined = undefined;

  constructor(params: IMelCloudParams) {
    super();
    const { username, password, interval } = params;
    this.devices = [];
    this.username = username;
    this.password = password;
    this.interval = interval;

    this.init();
  }

  async init() {
    try {
      const loginResponse: IClientLoginResponse = await this.login(
        this.username,
        this.password,
      );
      if (!loginResponse.LoginData) {
        throw Error('Login failed');
      }
      this.currentSessionData = loginResponse.LoginData;
      this.emit('login', { name: loginResponse.LoginData.Name });
      await this.fetchDevicesList();
    } catch (e) {
      this.emit('error', e);
    }
  }

  async setAta(
    deviceId: number,
    changes: IPrepareUpdate,
  ): Promise<IGetDeviceResponse | undefined> {
    const SET_ATA_URL =
      'https://app.melcloud.com/Mitsubishi.Wifi.Client/Device/SetAta';

    const opts = bodyParamsToFormPost({ ...changes, DeviceID: deviceId });
    try {
      const response = await fetch(SET_ATA_URL, {
        method: 'POST',
        body: opts,
        headers: {
          'X-MitsContextKey': this.currentSessionData?.ContextKey || '',
        },
      });

      const respContent: IGetDeviceResponse = <IGetDeviceResponse>(
        await response.json()
      );
      return respContent;
    } catch (e) {
      this.emit('error', e);
    }
  }

  async login(
    username: string,
    password: string,
  ): Promise<IClientLoginResponse> {
    const CLIENT_LOGIN_URL =
      'https://app.melcloud.com/Mitsubishi.Wifi.Client/Login/ClientLogin';

    const bodyParams: IClientLoginRequest = {
      AppVersion: MEL_CLOUD_APP_VERSION,
      CaptchaChallenge: '',
      CaptchaResponse: '',
      Email: username,
      Language: 0,
      Password: password,
      Persist: 'true',
    };

    const opts = bodyParamsToFormPost(bodyParams);

    try {
      const response = await fetch(CLIENT_LOGIN_URL, {
        method: 'POST',
        body: opts,
      });

      const respContent: IClientLoginResponse = <IClientLoginResponse>(
        await response.json()
      );
      return respContent;
    } catch (e) {
      this.emit('error', e);
      // TODO: limit retries
      return timeout(1000).then(() => this.login(username, password));
    }
  }

  async getDevice(
    id: number,
    building: number,
  ): Promise<IGetDeviceResponse | null> {
    const GET_DEVICE_URL = `https://app.melcloud.com/Mitsubishi.Wifi.Client/Device/Get?id=${id}&buildingID=${building}`;

    try {
      const response = await fetch(GET_DEVICE_URL, {
        method: 'GET',
        headers: {
          'X-MitsContextKey': this.currentSessionData?.ContextKey || '',
        },
      });
      const respContent: IGetDeviceResponse = <IGetDeviceResponse>(
        await response.json()
      );
      return respContent;
    } catch (e) {
      this.emit('error', e);
      return null;
    }
  }

  async attach(data: IDevice, location: ILocation) {
    const device = new Device(
      this,
      data,
      location,
      this.currentSessionData?.ContextKey || '',
    );

    // proxy device updates
    device.on('state', (...args) => this.emit('state', device, ...args));
    device.on('status', (...args) => this.emit('status', device, ...args));
    device.on('schedule', (...args) => this.emit('schedule', device, ...args));
    device.on('connect', (...args) => this.emit('device', device, ...args));
    device.on('error', (...args) => this.emit('device/error', device, ...args));

    await device.initialize();

    this.devices.push(device);
  }

  async fetchDevicesList() {
    const LIST_DEVICES_URL =
      'https://app.melcloud.com/Mitsubishi.Wifi.Client/User/ListDevices';

    try {
      const response = await fetch(LIST_DEVICES_URL, {
        method: 'GET',
        headers: {
          'X-MitsContextKey': this.currentSessionData?.ContextKey || '',
        },
      });

      const locations: ILocation[] = <ILocation[]>await response.json();
      const devices = locations.reduce(
        (
          result: { device: IDevice; location: ILocation }[],
          location: ILocation,
        ) => {
          location.Structure.Devices.forEach((device: IDevice) =>
            result.push({ device, location }),
          );

          location.Structure.Floors.forEach((floor) => {
            floor.Devices.forEach((device: IDevice) =>
              result.push({ device, location }),
            );

            floor.Areas.forEach((area) =>
              area.Devices.forEach((device: IDevice) =>
                result.push({ device, location }),
              ),
            );
          });

          location.Structure.Areas.forEach((area) =>
            area.Devices.forEach((device: IDevice) =>
              result.push({ device, location }),
            ),
          );

          return result;
        },
        [],
      );

      const attachedDevices = devices.map(({ device, location }) =>
        this.attach(device, location),
      );
      await Promise.all(attachedDevices);
    } catch (e) {
      this.emit('error', e);
    }
  }
}

export const melcloud = (args: IMelCloudParams) => new MelCloud(args);
