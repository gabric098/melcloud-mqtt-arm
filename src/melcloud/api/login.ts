export interface ILoginData {
  ContextKey: string;
  Client: number;
  Terms: number;
  AL: number;
  ML: number;
  CMI: boolean;
  IsStaff: boolean;
  CUTF: boolean;
  CAA: boolean;
  ReceiveCountryNotifications: boolean;
  ReceiveAllNotifications: boolean;
  CACA: boolean;
  CAGA: boolean;
  MaximumDevices: number;
  ShowDiagnostics: boolean;
  Language: number;
  Name: string;
  UseFahrenheit: boolean;
  Duration: number;
  Expiry: string;
  CMSC: boolean;
  PartnerApplicationVersion: string | null;
  EmailSettingsReminderShown: boolean;
  EmailUnitErrors: number;
  EmailCommsErrors: number;
  ChartSeriesHidden: number;
  IsImpersonated: boolean;
  LanguageCode: string;
  CurrencySymbol: string;
  SupportEmailAddress: string;
  DateSeperator: string;
  TimeSeparator: string;
  AtwLogoFile: string;
  DECCReport: boolean;
  CSVReport1min: boolean;
  HidePresetPanel: boolean;
  EmailSettingsReminderRequired: boolean;
  TermsText: string | null;
  MapView: boolean;
  MapZoom: number;
  MapLongitude: number;
  MapLatitude: number;
}

export interface IClientLoginResponse {
  ErrorId: number | null;
  ErrorMessage: string | null;
  LoginStatus: number;
  UserId: number;
  RandomKey: string | null;
  AppVersionAnnouncement: string | null;
  LoginData: ILoginData;
  ListPendingInvite: string[];
  ListOwnershipChangeRequest: string[];
  ListPendingAnnouncement: string[];
  LoginMinutes: number;
  LoginAttempts: number;
}

export type IClientLoginRequest = {
  AppVersion: string;
  CaptchaChallenge: string;
  CaptchaResponse: string;
  Email: string;
  Language: number;
  Password: string;
  Persist: string;
};
