import { connect } from 'mqtt';
import { melcloud } from './melcloud/index';
import { config } from './config';
import { topics } from './constants';
import { version } from '../package.json';
import { Device } from './melcloud/device';
import { log, error } from './melcloud/utils';

const mqttClient = connect(config.mqtt.host, {
  username: config.mqtt.username,
  password: config.mqtt.password,
  clientId: config.mqtt.id,
  will: {
    topic: topics.state(),
    payload: JSON.stringify({ online: false }),
    retain: true,
    qos: 2,
  },
});

const melcloudClient = melcloud({
  username: config.melcloud.username,
  password: config.melcloud.password,
  interval: config.melcloud.interval,
});

const subsciptions: Map<string, Device> = new Map();

mqttClient.on('connect', () => log('mqtt', `connected to ${config.mqtt.host}`));

melcloudClient.on('login', () => {
  // TODO: handle name data
  log('melcloud', `logged in as ${config.melcloud.username}`);
  log('melcloud', `polling interval is ${config.melcloud.interval}ms`);

  mqttClient.publish(
    topics.state(),
    JSON.stringify({
      online: true,
      version,
    }),
    { retain: true },
  );
});

melcloudClient.on('device', (device: Device) => {
  const deviceId = device.id.toString();
  const topic = topics.change(deviceId);
  mqttClient.subscribe(topic);
  subsciptions.set(topic, device);

  log('melcloud', `registed device at ${topics.update(deviceId)}`);

  mqttClient.publish(topics.device(), JSON.stringify(device.info), {
    retain: true,
  });

  mqttClient.publish(topics.info(deviceId), JSON.stringify(device.info), {
    retain: true,
  });
});

melcloudClient.on(
  'state',
  (
    device: Device,
    state: Record<string, unknown>,
    diff: Record<string, unknown>,
  ) => {
    const deviceId = device.id.toString();
    log('melcloud', `received state for ${topics.update(deviceId)}`);
    log('melcloud', `  > ${JSON.stringify(diff)}`);

    mqttClient.publish(topics.update(deviceId), JSON.stringify(state), {
      retain: true,
    });
  },
);

melcloudClient.on(
  'status',
  (
    device: Device,
    state: Record<string, unknown>,
    diff: Record<string, unknown>,
  ) => {
    const deviceId = device.id.toString();
    log('melcloud', `received status for ${topics.update(deviceId)}`);
    log('melcloud', `  > ${JSON.stringify(diff)}`);

    mqttClient.publish(topics.status(deviceId), JSON.stringify(state), {
      retain: true,
    });
  },
);

melcloudClient.on(
  'schedule',
  (device: Device, state: Record<string, unknown>) => {
    // log('melcloud', `schedule for ${topics.update(device.id, device.building)}`);
    // log('melcloud', `  > ${JSON.stringify(diff)}`);

    mqttClient.publish(
      topics.schedule(device.id.toString()),
      JSON.stringify(state),
    );
  },
);

mqttClient.on('message', (topic: string, data: Record<string, unknown>) => {
  const device = subsciptions.get(topic);

  if (!device) {
    error('mqtt', `received data for unknown device ${topic}`);
    return;
  }

  try {
    log('mqtt', `received update for ${topic}`);
    log('mqtt', `  > ${data.toString()}`);

    device.set(JSON.parse(data.toString()));
  } catch (e) {
    error('mqtt', 'not able to parse incoming message');
  }
});

mqttClient.on('error', (e: Error) => {
  error('mqtt', 'connection error');
  error('mqtt', `  > ${e.toString()}`);
});

melcloudClient.on('error', (e: Error) => {
  error('melcloud', 'unexpected error');
  error('melcloud', `  > ${e.toString()}`);
});

melcloudClient.on('device/error', (device: Device, e: Error) => {
  error('melcloud', 'unexpected device error');
  error('melcloud', `  > ${e.toString()}`);
});
