import { config } from './config';

export const MEL_CLOUD_APP_VERSION = '1.22.1.0';

export const topics: { [name: string]: (id?: string) => string } = {
  state: () => `${config.mqtt.path}/state`,
  device: () => `${config.mqtt.path}/device`,
  update: (id?: string) => `${config.mqtt.path}/${id}`,
  status: (id?: string) => `${config.mqtt.path}/${id}/status`,
  info: (id?: string) => `${config.mqtt.path}/${id}/info`,
  diff: (id?: string) => `${config.mqtt.path}/${id}/diff`,
  schedule: (id?: string) => `${config.mqtt.path}/${id}/schedule`,
  change: (id?: string) => `${config.mqtt.path}/${id}/set`,
};
